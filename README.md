# WEmu

Oh boy, I don't know what I'm doing. I don't use git, so I'm a little lost right now. Anyway, here is where I'm keeping my language files for WEmu. If you would like to submit translations, I'd really appreciate it. You can send them to wemu at cock.li.

I haven't decided on making WEmu open yet, as it's a mess inside still and I don't know what I want to do with it in general. It's getting to the stage where having a few pairs of eyes on it might be good for it. Please see the Wiki for more information.

If you have any questions or anything, you can direct them to wemu at cock.li.

2019-06-29 - WEmu is finally available - check the files to download.
2019-07-13 - Some updates, forgot to do some dialog replacing in the emailer. Added choose-your-own language support in options (English, 日本語, and Ym Prat).

Name: WEmu.exe
Size: 1046016 bytes (1021 KiB)
CRC32: 9527AACB
CRC64: E3E3366CB99F7336
SHA256: 4029DB5078DC98CA153D4D27ABF723DE3804BFCCE9B3E0152F7D8C381A0C25B2
SHA1: 30A1EBD5612BF0420474107D2A45ADB3D5126FF5
BLAKE2sp: B308302A5B4C267120C1925DBDD0740886A1BA0723E1F014A54A3FBE4B8FB562


Name: WCardBuilder.exe
Size: 193024 bytes (188 KiB)
CRC32: A259A52C
CRC64: 527173EBD142FC7A
SHA256: 90728356FF817488ADDA08D5604C1FB9E26FC4CEA2F9265CF6E237BDDB80F2BE
SHA1: B284FA7EDE3C6B60B33A44657FBE0891143A4335
BLAKE2sp: B790D916E166FA06D5E47428B91578510E5868F96510A5D9392F43DC6345ECE2


Name: ProfileBuilder.exe
Size: 84480 bytes (82 KiB)
CRC32: EB28F721
CRC64: 00FAF12466A86CCA
SHA256: A3EF91E3C7AC938E28C17A0E58B747903FC85722FB9951AC6CA88E692FBE3D55
SHA1: 0738F8888D42E2E9CDCBCDC381D15C99D6BB3890
BLAKE2sp: 3A8BB3F60885978243F90BDE1BEFDAF3313BD72B037D278EB732982892C966D8
